package kmr.game.game;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import kmr.game.R;

public class WinOrGameOverDialog extends DialogFragment implements View.OnClickListener{
    Game game;
    public WinOrGameOverDialog() {
    }

    @SuppressLint("ValidFragment")
    public WinOrGameOverDialog(Game game) {
        this.game = game;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gameoverorwin_dialogframe, null);
        View v2 = inflater.inflate(R.layout.gameoverorwin_dialogframe, null);
        v.findViewById(R.id.bt_replay).setOnClickListener(this);
        v2.findViewById(R.id.bt_backtomenu).setOnClickListener(this);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        TextView textView = v.findViewById(R.id.textView1);
        textView.setText(getArguments().getString("outcomeGame"));
        TextView textViewScore = v.findViewById(R.id.textViewScore);
        textViewScore.setText("" + getArguments().getInt("score"));

        return v;
    }

    public void onClick(View v) {
        game.reInitialize();
        game.createField();
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
