package kmr.game.game;

import java.util.Vector;

import kmr.game.cell.Cell;
import kmr.game.cell.Coordinate;

import static kmr.game.cell.Color.Black;
import static kmr.game.cell.Color.Red;
import static kmr.game.cell.Color.Simple;

public class Field implements Cloneable {

    private int sizeRow;
    private int sizeColumn;
    private Cell fieldCells[][];
    private Coordinate coordinatePlayer = new Coordinate(0, 0);
    private Coordinate coordinateGoalCell = new Coordinate(0, 0);

    public Field(int row, int column) {
        this.sizeRow = row;
        this.sizeColumn = column;
        this.fieldCells = new Cell[sizeRow][sizeColumn];
        for (int i = 0; i < sizeRow; i++) {
            for (int j = 0; j < sizeColumn; j++) {
                this.fieldCells[i][j] = new Cell();
            }
        }
    }

    public Field(int array[][]) {
        this.sizeRow = array.length;
        this.sizeColumn = array[0].length;
        this.fieldCells = new Cell[sizeRow][sizeColumn];
        for (int i = 0; i < sizeRow; i++) {
            for (int j = 0; j < sizeColumn; j++) {
                this.fieldCells[i][j] = new Cell();
                this.fieldCells[i][j].setValue(array[i][j]);
            }
        }
    }

    public Cell getCell(int n, int m) {
        return fieldCells[n][m];
    }

    public int getPicture(int n, int m) {
        return fieldCells[n][m].getPicture();
    }

    public void setSimpleCell(Coordinate coordinate, int value) {
        int x = coordinate.getX();
        int y = coordinate.getY();
        this.fieldCells[x][y].setPicture(new SimpleNumbers().get());
        this.fieldCells[x][y].setColor(Simple);
        this.fieldCells[x][y].setValue(value);
    }

    public void setPlayerCell(Coordinate coordinate, int value) {
        int x = coordinate.getX();
        int y = coordinate.getY();
        this.fieldCells[x][y].setPicture(new BlackNumbers().get());
        this.fieldCells[x][y].setValue(value);
        this.fieldCells[x][y].setColor(Black);
        coordinatePlayer.setXY(x, y);
    }

    public Coordinate getCoordinatePlayer() {
        return coordinatePlayer;
    }

    public void setGoalCell(Coordinate coordinate, int value) {
        int x = coordinate.getX();
        int y = coordinate.getY();
        this.fieldCells[x][y].setPicture(new RedNumbers().get());
        this.fieldCells[x][y].setValue(value);
        this.fieldCells[x][y].setColor(Red);
        coordinateGoalCell.setXY(x, y);
    }

    public Coordinate getCoordinateGoalCell() {
        return coordinateGoalCell;
    }

    public Field Clone() throws
            CloneNotSupportedException {
        Field obj = (Field) super.clone();
        for (int i = 0; i < sizeRow; i++) {
            for (int j = 0; j < sizeColumn; j++) {
                obj.fieldCells[i][j] = fieldCells[i][j].Clone();
            }
        }
        return obj;
    }

    public int[] getRow(int row) {
        int result[] = new int[sizeColumn];
        for (int indexColumn = 0; indexColumn < sizeColumn; indexColumn++) {
            result[indexColumn] = fieldCells[row][indexColumn].getValue();
        }
        return result;
    }

    public void setRow(Vector<Integer> vector, int row) {
        for (int indexColumn = 0; indexColumn < sizeColumn; indexColumn++) {
            fieldCells[row][indexColumn].setValue(vector.get(indexColumn));
        }
    }

    public int[] getColumn(int column) {
        int result[] = new int[sizeRow];
        for (int indexRow = 0; indexRow < sizeRow; indexRow++) {
            result[indexRow] = fieldCells[indexRow][column].getValue();
        }
        return result;
    }

    public void setColumn(Vector<Integer> vector, int column) {
        for (int indexRow = 0; indexRow < sizeRow; indexRow++) {
            fieldCells[indexRow][column].setValue(vector.get(indexRow));
        }
    }

    public int getSizeRow() {
        return sizeRow;
    }

    public int getSizeColumn() {
        return sizeColumn;
    }

    public Vector<Cell> getRowCell(int row) {
        Vector<Cell> result = new Vector<Cell>(sizeColumn);
        for (int indexColumn = 0; indexColumn < sizeColumn; indexColumn++) {
            result.add(indexColumn, fieldCells[row][indexColumn]);
        }
        return result;
    }

    public void setRowCell(Vector<Cell> vector, int row) {
        Cell[] arr = new Cell[sizeColumn];
        for (int i = 0; i < sizeColumn; i++) {
            arr[i] = new Cell();
            arr[i].setValue(vector.get(i).getValue());
            arr[i].setColor(vector.get(i).getColor());
        }
        fieldCells[row] = arr;
    }

    public Vector<Cell> getColumnCell(int column) {
        Vector<Cell> result = new Vector<Cell>(sizeRow);
        for (int indexRow = 0; indexRow < sizeRow; indexRow++) {
            result.add(indexRow, fieldCells[indexRow][column]);
        }
        return result;
    }

    public void setColumnCell(Vector<Cell> vector, int column) {
        Cell[] arr = new Cell[sizeColumn];
        for (int i = 0; i < sizeColumn; i++) {
            arr[i] = new Cell();
            arr[i].setValue(vector.get(i).getValue());
            arr[i].setColor(vector.get(i).getColor());
        }
        for (int indexRow = 0; indexRow < sizeRow; indexRow++) {
            fieldCells[indexRow][column] = arr[indexRow];
        }
    }

    public boolean isEmptyCell(int x, int y) {
        return getCell(x, y).getValue() == 0;
    }

    private void printVector(Vector<Cell> vector) {
        for (int i = 0; i < vector.size(); i++) {
            Cell object = vector.get(i);
            System.out.print(object.getValue() + " ");
        }
    }

    public Cell[][] getFieldCells() {
        return fieldCells;
    }

    public void setFieldCells(Cell[][] fieldCells) {
        this.fieldCells = fieldCells;
    }

    public void setCell(int indexRow, int indexColumn, Cell cell) {
        this.fieldCells[indexRow][indexColumn] = cell;
    }
}