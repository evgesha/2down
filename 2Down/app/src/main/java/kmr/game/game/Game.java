package kmr.game.game;

import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import kmr.game.moveOnMatriz.MoveActivity;
import kmr.game.R;
import kmr.game.fieldCreatorPackage.CellFiller;
import kmr.game.fieldCreatorPackage.FieldCreator;
import kmr.game.view.TextViewPlus;

public class Game extends AppCompatActivity implements View.OnTouchListener {

    public final static String GAME_SIZE = "GAME_SIZE";
    private int ROW;
    private int COLUMN;
    private DialogFragment dialogFragment;
    private Field gameField;
    private int[][] id;
    private TableLayout tableLayout;
    private boolean mIsBound = false;
    private MusicService musicService;
    private static MusicService tmpMusicService;
    private ServiceConnection mServiceConnection;
    private static Records records;
    private WinOrGameOverDialog winOrGameOverDialog;
    private Bundle gameOver;
    private Bundle gameWin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startInitialize();
        createField();
        tableLayout.setOnTouchListener(this);
    }

    public void startInitialize() {
        int[] gameSize = getIntent().getIntArrayExtra(GAME_SIZE);
        ROW = gameSize[0];
        COLUMN = gameSize[1];
        dialogFragment = new PauseMenu();
        setContentView(R.layout.activity_five_game);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        winOrGameOverInit();
        createMusic();
        initRecords();
    }

    public void reInitialize() {
        tableLayout.removeAllViews();
        int[] gameSize = getIntent().getIntArrayExtra(GAME_SIZE);
        ROW = gameSize[0];
        COLUMN = gameSize[1];
    }

    private void createMusic() {
        mServiceConnection = new ServiceConnection() {

            public void onServiceConnected(ComponentName name, IBinder
                    binder) {
                musicService = ((MusicService.ServiceBinder) binder).getService();
            }

            public void onServiceDisconnected(ComponentName name) {
                musicService = null;
            }
        };
        startService(new Intent().setClass(this, MusicService.class));
        doBindService();
    }

    private void doBindService() {
        bindService(new Intent(this, MusicService.class),
                mServiceConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService() {
        if (mIsBound) {
            unbindService(mServiceConnection);
            mIsBound = false;
        }
    }

    private void winOrGameOverInit() {
        winOrGameOverDialog = new WinOrGameOverDialog(this);
        gameOver = new Bundle();
        gameWin = new Bundle();
        gameOver.putString("outcomeGame", "Game Over");
        gameWin.putString("outcomeGame", "You are win!");
    }

    protected void resumeMusic() {
        if (musicService != null) {
            musicService.resumeMusic();
        }
    }

    private void initRecords() {
        TextViewPlus textViewPlus = (TextViewPlus) findViewById(R.id.tv_Records);
        records = new Records(textViewPlus);
    }

    public static synchronized void setRecords(int value) {
        records.setRecords(value);
    }

    public void createField() {
        id = new int[ROW][COLUMN];

        fillField(ROW, COLUMN, id);
    }

    public void fillField(int rows, int columns, int[][] id) {
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        gameField = createGameField();

        for (int curRow = 0; curRow < rows; curRow++) {
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tableRow.setGravity(Gravity.CENTER_HORIZONTAL);

            createCell(columns, id, curRow, tableRow);

            tableLayout.addView(tableRow, curRow);
        }
    }

    public void createCell(int columns, int[][] id, int curRow, TableRow tableRow) {
        for (int curColum = 0; curColum < columns; curColum++) {
            ImageView imageView = new ImageView(this);
//            imageView.setMinimumHeight(setConfigure(R.string.sizeCells));
//            imageView.setMinimumWidth(setConfigure(R.string.sizeCells));
            imageView.setPadding(setConfigure(R.string.padding_left), setConfigure(R.string.padding_top), setConfigure(R.string.padding_right), setConfigure(R.string.padding_bottom));
            imageView.setImageResource(gameField.getPicture(curRow, curColum));
            imageView.setId(imageView.generateViewId());
            id[curRow][curColum] = imageView.getId();
            tableRow.addView(imageView, curColum);
        }
    }

    public int setConfigure(int stringFromConfigure) {
        return Integer.parseInt(getString(stringFromConfigure));
    }

    public void BackToMenu(View view) {
        finish();
        //startActivity(new Intent(this, App.class));
    }

    public void GameOver(View view) {
        //startActivity(new Intent(this, GameOverOrWin.class).putExtra(GAME_SIZE, new int[]{ROW, COLUMN}));

        gameOver.putInt("score", records.getRecords());
        winOrGameOverDialog.setArguments(gameOver);
        winOrGameOverDialog.show(getFragmentManager(), "Game Over");
    }

    public void GameWin(View view) {
        gameWin.putInt("score", records.getRecords());
        winOrGameOverDialog.setArguments(gameWin);
        winOrGameOverDialog.show(getFragmentManager(), "Game Win");
    }


    public void OnOffMusic(View view) {

    }

    public void Pause(View view) {
        dialogFragment.show(getFragmentManager(), "dialogFragment");
        tmpMusicService = musicService;
        musicService.pauseMusic();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        MoveActivity activity = MoveActivity.getInstance(view.getContext());
        if (gameField != null) {
            try {
                activity.onTouchEvent(motionEvent, gameField);
                CellFiller filler = new CellFiller();
                gameField = filler.fill(gameField);
                redrawField();
            } catch (CloneNotSupportedException e) {
                //todo Как-то обработать ошибку
            }
        }
        return true;
    }

    private Field createGameField() {
        return new FieldCreator().create(ROW, COLUMN);
    }

    private void redrawField() {
        if (gameField == null) {
            return;
        }
        for (int row = 0; row < ROW; row++) {
            for (int column = 0; column < COLUMN; column++) {
                ((ImageView) tableLayout.findViewById(id[row][column])).setImageResource(gameField.getPicture(row, column));
            }
        }
    }

    public void activeButtons() {
        findViewById(R.id.start_ten).setClickable(true);
        findViewById(R.id.start_five).setClickable(true);
    }

    public static synchronized MusicService getTmpMusicService() {
        return tmpMusicService;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (musicService != null) {
            musicService.resumeMusic();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (musicService != null) {
            musicService.pauseMusic();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (musicService != null) {
            musicService.startMusic();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (musicService != null) {
            musicService.tmpStopMusic();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (musicService != null) {
            musicService.resumeMusic();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (musicService != null) {
            musicService.stopMusic();
        }
        stopService(new Intent().setClass(this, MusicService.class));
        doUnbindService();
    }
}