package kmr.game.game.creators;

import android.util.SparseIntArray;

import java.util.Random;

import kmr.game.game.BlackNumbers;
import kmr.game.game.RedNumbers;
import kmr.game.game.SimpleNumbers;

class RandomImageGetter {

    private BlackNumbers blue = new BlackNumbers();
    private RedNumbers red = new RedNumbers();
    private SimpleNumbers simple = new SimpleNumbers();

    private Random random = new Random();

    private int COUNT_NUMBS = 3;

    public int getRandImage() {
        SparseIntArray result = getRandNumbs();
        return result.valueAt(random.nextInt(result.size()));
    }

    public SparseIntArray getRandNumbs() {
        switch (random.nextInt(COUNT_NUMBS)) {
            case 0: return blue.get();
            case 1: return red.get();
            case 2: return simple.get();
        }
        return simple.get();
    }

}
