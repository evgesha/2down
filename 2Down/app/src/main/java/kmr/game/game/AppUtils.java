package kmr.game.game;

import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

/**
 * Утилитарный класс по работе с экраном
 */
public class AppUtils {

    private DisplayMetrics displaymetrics = new DisplayMetrics();

    public AppUtils(AppCompatActivity app) {
        app.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    }

    /**
     * @return ширина экрана
     */
    public int getScreenWidth() {
        return displaymetrics.widthPixels;
    }


    /**
     * @return высота экрана
     */
    public int getScreenHeight() {
        return displaymetrics.heightPixels;
    }

}
