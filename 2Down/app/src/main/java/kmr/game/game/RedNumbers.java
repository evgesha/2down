package kmr.game.game;

import android.util.SparseIntArray;

import kmr.game.R;

public class RedNumbers {
    private static final SparseIntArray RED_NUMBERS = new SparseIntArray() {{
        put(2, R.drawable.square_red_2);
        put(4, R.drawable.square_red_4);
        put(8, R.drawable.square_red_8);
        put(16, R.drawable.square_red_16);
        put(32, R.drawable.square_red_32);
        put(64, R.drawable.square_red_64);
        put(128, R.drawable.square_red_128);
        put(256, R.drawable.square_red_256);
        put(512, R.drawable.square_red_512);
        put(1024, R.drawable.square_red_1024);
        put(2048, R.drawable.square_red_2048);
        put(4096, R.drawable.square_red_4096);
        put(8192, R.drawable.square_red_8192);
    }};

    public SparseIntArray get(){
        return this.RED_NUMBERS;
    }
}
