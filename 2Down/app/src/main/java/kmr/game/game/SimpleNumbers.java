package kmr.game.game;

import android.util.SparseIntArray;

import kmr.game.R;

public class SimpleNumbers {
    public static final SparseIntArray SIMPLE_NUMBERS = new SparseIntArray() {{
        put(2, R.drawable.square_white_2);
        put(4, R.drawable.square_white_4);
        put(8, R.drawable.square_white_8);
        put(16, R.drawable.square_white_16);
        put(32, R.drawable.square_white_32);
        put(64, R.drawable.square_white_64);
        put(128, R.drawable.square_white_128);
        put(256, R.drawable.square_white_256);
        put(512, R.drawable.square_white_512);
        put(1024, R.drawable.square_white_1024);
        put(2048, R.drawable.square_white_2048);
        put(4096, R.drawable.square_white_4096);
        put(8192, R.drawable.square_white_8192);
    }};

    public SparseIntArray get(){
        return this.SIMPLE_NUMBERS;
    }
}
