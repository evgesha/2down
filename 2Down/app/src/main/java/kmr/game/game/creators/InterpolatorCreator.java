package kmr.game.game.creators;

import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InterpolatorCreator {

    private static final List<Interpolator> interpolators = new ArrayList<Interpolator>(){{
        add(new LinearInterpolator());
//        add(new OvershootInterpolator());
//        add(new LinearOutSlowInInterpolator());
//        add(new AccelerateInterpolator());
//        add(new DecelerateInterpolator());
//        add(new AccelerateDecelerateInterpolator());
//        add(new AnticipateInterpolator());
//        add(new AnticipateOvershootInterpolator());
        }};


    public Interpolator get() {
        return interpolators.get(new Random().nextInt(interpolators.size()));
    }
}
