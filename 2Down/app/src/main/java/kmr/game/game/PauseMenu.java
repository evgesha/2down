package kmr.game.game;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import kmr.game.R;

public class PauseMenu extends DialogFragment implements View.OnClickListener {
    private MusicService musicService;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog, null);
        v.findViewById(R.id.bt_return).setOnClickListener(this);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        musicService = Game.getTmpMusicService();
        return v;
    }

    public void onClick(View v) {
        musicService.resumeMusic();
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        musicService.resumeMusic();
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        musicService.resumeMusic();
        super.onCancel(dialog);
    }
}