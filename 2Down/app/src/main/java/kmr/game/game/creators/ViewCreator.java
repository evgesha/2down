package kmr.game.game.creators;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

import kmr.game.game.App;
import kmr.game.game.AppUtils;

public class ViewCreator {

    private AppCompatActivity app;
    private RandomImageGetter getter = new RandomImageGetter();
    private Random random = new Random();

    public ViewCreator(App app) {
        this.app = app;
    }

    public View create() {
        ImageView view = new ImageView(app);
        view.setBackgroundResource(getter.getRandImage());
        int size = genMinimumSize();
        view.setMinimumHeight(size);
        view.setMinimumWidth(size);
        AppUtils utils = new AppUtils(app);
        view.setY(utils.getScreenHeight() + 20);
        int screenWidth = utils.getScreenWidth();
        view.setX(random.nextInt(2) == 1 ? screenWidth / 2 : screenWidth / 3);
        return view;
    }

    private int genMinimumSize() {
        return random.nextInt(350) + 20;
    }
}
