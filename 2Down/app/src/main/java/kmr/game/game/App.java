package kmr.game.game;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;


import java.util.Timer;
import java.util.TimerTask;

import kmr.game.R;
import kmr.game.game.animation.FallSquaresAnimator;
import kmr.game.game.creators.ViewCreator;

public class App extends AppCompatActivity {

    public final static String GAME_SIZE = "GAME_SIZE";
    private RelativeLayout layout;
    private Integer PERIOD_FALL_SQUARE = 2000;
    private FallSquaresAnimator animator =  new FallSquaresAnimator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.main_menu);
        layout = (RelativeLayout) findViewById(R.id.layout);
        startSquaresFall();
    }

    public void startFiveGame(View view) {
        start(new int[]{5, 5});
    }

    public void startTenGame(View view) {
        start(new int[]{10, 10});
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void start(int[] size) {
        startActivity(new Intent(this, Game.class).putExtra(GAME_SIZE, size));
    }

    private void startSquaresFall() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            animateFallSquare();
                        } catch (InterruptedException e) {
                            // do nothing
                        }
                    }
                });
            }
        }, 0, PERIOD_FALL_SQUARE);
    }

    private void animateFallSquare() throws InterruptedException {
        View view = new ViewCreator(this).create();
        animator.animate(view);
        layout.addView(view);
    }

    @Override
    public void onResume(){
        super.onResume();
    }
}
