package kmr.game.game;

import kmr.game.view.TextViewPlus;

public class Records {
    TextViewPlus textView;

    public Records(TextViewPlus textViewPlus) {
        textView = textViewPlus;
        textView.setText(0+"");
    }

    public void setRecords(int numbCell) {
//        String stringRecords = String.valueOf(textView.getText());
//        int tmpRecords = Integer.parseInt(stringRecords);
//        tmpRecords += numbCell;
        textView.setText(numbCell+"");
    }

    public int getRecords(){
        String stringRecords = String.valueOf(textView.getText());
        return Integer.parseInt(stringRecords);
    }
}
