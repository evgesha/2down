package kmr.game.game;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import kmr.game.R;

public class GameOverOrWin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiry_gameover_or_win);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void BackToMenu(View view){
        startActivity(new Intent(this, App.class));
    }

    public void Replay(View view){
        startActivity(new Intent(this, Game.class).putExtra("GAME_SIZE", getIntent().getIntArrayExtra("GAME_SIZE")));
    }
}
