package kmr.game.game.animation;

import android.view.View;

import java.util.Random;

import kmr.game.game.creators.InterpolatorCreator;


public class FallSquaresAnimator {

    private Long DURATION_ANIMATION = 15000L;
    private InterpolatorCreator interpolatorCreator = new InterpolatorCreator();
    private Random random = new Random();

    public void animate(View view) {
        view.animate()
                .translationY(-2000)
                .translationX(getEndPoint())
                .rotationBy(getRotation())
                .setDuration(DURATION_ANIMATION)
                .setInterpolator(interpolatorCreator.get())
                .start();
    }

    private int getEndPoint() {
        return random.nextInt(1000);
    }

    private int getRotation() {
        int result = random.nextInt(400);
        return random.nextInt(1) == 0 ? -result : result;
    }
}
