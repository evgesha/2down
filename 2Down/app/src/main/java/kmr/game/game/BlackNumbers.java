package kmr.game.game;

import android.util.SparseIntArray;

import kmr.game.R;

public class BlackNumbers {
    private static final SparseIntArray BLACK_NUMBERS = new SparseIntArray() {{
        put(2, R.drawable.square_black_2);
        put(4, R.drawable.square_black_4);
        put(8, R.drawable.square_black_8);
        put(16, R.drawable.square_black_16);
        put(32, R.drawable.square_black_32);
        put(64, R.drawable.square_black_64);
        put(128, R.drawable.square_black_128);
        put(256, R.drawable.square_black_256);
        put(512, R.drawable.square_black_512);
        put(1024, R.drawable.square_black_1024);
        put(2048, R.drawable.square_black_2048);
        put(4096, R.drawable.square_black_4096);
        put(8192, R.drawable.square_black_8192);
    }};

    public SparseIntArray get(){
        return this.BLACK_NUMBERS;
    }
}
