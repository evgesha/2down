package kmr.game.moveOnMatriz;

import kmr.game.game.Field;

public class MatrixMoveDown extends MatrixMove {
    public MatrixMoveDown(Field field) {
        super(field);
    }

    public void move() {
        transpose();
        reverse();
        colorShift(amount());
        shift();
        reverse();
        shift();
        transpose();
    }
}
