package kmr.game.moveOnMatriz;

import kmr.game.cell.Cell;
import kmr.game.game.BlackNumbers;
import kmr.game.game.Field;
import kmr.game.game.Game;
import kmr.game.game.RedNumbers;

import static kmr.game.cell.Color.Black;
import static kmr.game.cell.Color.Red;
import static kmr.game.cell.Color.Simple;

public abstract class MatrixMove {
    private static int sum = 0;
    private Field field;

    public MatrixMove(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public int[][] getFieldOnlyValue() {
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        int[][] arrValue = new int[row][column];
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = 0; indexColumn < column; indexColumn++) {
                arrValue[indexRow][indexColumn] = this.field.getCell(indexRow, indexColumn).getValue();
            }
        }
        return arrValue;
    }

    public void setFieldOnlyValue(int[][] arrValue) {
        for (int indexRow = 0; indexRow < this.field.getSizeRow(); indexRow++) {
            for (int indexColumn = 0; indexColumn < this.field.getSizeColumn(); indexColumn++) {
                this.field.getFieldCells()[indexRow][indexColumn].setValue(arrValue[indexRow][indexColumn]);
            }
        }
    }

    public void reverse() {
        int max_index;
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        max_index = row / 2 + row % 2;
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = 0; indexColumn < max_index; indexColumn++) {
                Cell save = field.getCell(indexRow, indexColumn);
                field.setCell(indexRow, indexColumn, field.getCell(indexRow, row - indexColumn - 1)); //разобраться с цветом
                field.setCell(indexRow, column - indexColumn - 1, save);
            }
        }
    }

    public void transpose() {
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = indexRow + 1; indexColumn < column; indexColumn++) {
                Cell save = field.getCell(indexRow, indexColumn);
                field.setCell(indexRow, indexColumn, field.getCell(indexColumn, indexRow));
                field.setCell(indexColumn, indexRow, save);
            }
        }
    }

    public int[][] amount() {
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        int[][] arrValue = getFieldOnlyValue();
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = 0; indexColumn < column - 1; indexColumn++) {
                if (arrValue[indexRow][indexColumn] == arrValue[indexRow][indexColumn + 1]) {
                    sum += arrValue[indexRow][indexColumn];
                    Game.setRecords(sum);
                    arrValue[indexRow][indexColumn + 1] *= 2;
                    arrValue[indexRow][indexColumn] = 0;
                    indexColumn++;
                }
            }
        }
        return arrValue;
    }

    public void shift() {
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = 1; indexColumn < column; indexColumn++) {
                if (field.getCell(indexRow, indexColumn).getValue() == 0) {
                    for (int poz = indexColumn; poz > 0; poz--) {
                        field.setCell(indexRow, poz, field.getCell(indexRow, poz - 1));
                    }
                    field.setCell(indexRow, 0, new Cell(0, Simple));
                }
            }
        }
    }

    public void colorShift(int[][] arrValue) {
        int row = this.field.getSizeRow();
        int column = this.field.getSizeColumn();
        for (int indexRow = 0; indexRow < row; indexRow++) {
            for (int indexColumn = 0; indexColumn < column - 1; indexColumn++) {
                if (arrValue[indexRow][indexColumn] == 0 && field.getCell(indexRow, indexColumn).getColor() != Simple) {
                   Cell newColorCell = field.getCell(indexRow, indexColumn + 1);
                   Cell oldColorCell = field.getCell(indexRow, indexColumn);
                   newColorCell.setColor(oldColorCell.getColor());
                   if (newColorCell.getColor() == Red) {
                       newColorCell.setPicture(new RedNumbers().get());
                   }
                   if (newColorCell.getColor() == Black) {
                       newColorCell.setPicture(new BlackNumbers().get());
                   }
                   oldColorCell.setColor(Simple);
                   field.setCell(indexRow, indexColumn + 1, oldColorCell);
                   field.setCell(indexRow, indexColumn + 1, newColorCell);
                }
            }
        }
        setFieldOnlyValue(arrValue);
    }

    public abstract void move();
}
