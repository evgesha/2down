package kmr.game.moveOnMatriz;

import kmr.game.game.Field;

public class MatrixMoveRight extends MatrixMove {
    public MatrixMoveRight(Field field) {
        super(field);
    }

    public void move() {
        reverse();
        colorShift(amount());
        reverse();
        shift();
    }
}
