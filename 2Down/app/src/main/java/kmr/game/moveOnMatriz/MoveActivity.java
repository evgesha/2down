package kmr.game.moveOnMatriz;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

import kmr.game.game.Field;

public class MoveActivity extends View {
    private float lastX;
    private float lastY;
    private float diffX;
    private float diffY;
    private float startX, startY;
    private static MoveActivity instance;
    private MatrixMove matrixMove;
    private int eps = 5;

    private MoveActivity(Context context) {
        super(context);
    }

    public static MoveActivity getInstance(Context context){
        if (instance == null){
            return instance = new MoveActivity(context);
        } else {
            return instance;
        }
    }

    public void choiceRight(Field object) throws CloneNotSupportedException {
        matrixMove = new MatrixMoveRight(object);
        matrixMove.move();
    }

    public void choiceLeft(Field object) throws CloneNotSupportedException {
        matrixMove = new MatrixMoveLeft(object);
        matrixMove.move();
    }

    public void choiceUp(Field object) throws CloneNotSupportedException {
        matrixMove = new MatrixMoveUp(object);
        matrixMove.move();
    }

    public void choiceDown(Field object) throws CloneNotSupportedException {
        matrixMove = new MatrixMoveDown(object);
        matrixMove.move();
    }

    public boolean onTouchEvent(MotionEvent event, Field field) throws CloneNotSupportedException {
        final int action = event.getAction();
        final float x = event.getX();
        final float y = event.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                startX = x;
                startY = y;
                lastX = x;
                lastY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                final float deltaX = Math.abs(lastX - x);
                final float deltaY = Math.abs(lastY - y);
                lastX = x;
                lastY = y;
                diffX = deltaX;
                diffY = deltaY;
                break;
            case MotionEvent.ACTION_UP:
                if (diffX > diffY) {
                    if (lastX > startX + eps) {
                        choiceRight(field);
                    }
                    if (lastX < startX - eps) {
                        choiceLeft(field);
                    }
                } else {
                    if (lastY < startY - eps) {
                        choiceUp(field);
                    }
                    if (lastY > startY + eps) {
                        choiceDown(field);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }
}