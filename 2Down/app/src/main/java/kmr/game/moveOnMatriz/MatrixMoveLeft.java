package kmr.game.moveOnMatriz;

import kmr.game.game.Field;

public class MatrixMoveLeft extends MatrixMove {
    public MatrixMoveLeft(Field field) {
        super(field);
    }

    public void move() {
        colorShift(amount());
        shift();
        reverse();
        shift();
        reverse();
    }
}
