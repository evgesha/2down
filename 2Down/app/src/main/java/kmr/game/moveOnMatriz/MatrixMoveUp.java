package kmr.game.moveOnMatriz;

import kmr.game.game.Field;

public class MatrixMoveUp extends MatrixMove {
    public MatrixMoveUp(Field field) {
        super(field);
    }

    public void move() {
        transpose();
        colorShift(amount());
        shift();
        transpose();
    }
}
