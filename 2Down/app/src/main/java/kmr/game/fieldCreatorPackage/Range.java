package kmr.game.fieldCreatorPackage;

import java.util.Random;

public enum Range {
    //Todo Пока по 3 строки, дальше нужно придумать как сделать,
    // чтобы рандомилось в зависимости от размерности поля

    FIRST_LEVEL(new int[]{2, 4, 8}, 3),
    SECOND_LEVEL(new int[]{16, 32, 64}, 6),
    THIRD_LEVEL(new int[]{128, 256, 512}, 9),
    LAST_LEVEL(new int[]{1024, 2048, 4096}, 12);

    private int[] values;
    private int range;

    Range(int[] values, int range) {
        this.values = values;
        this.range = range;
    }

    public int getValue() {
        return values[getRandom()];
    }

    public int getRange() {
        return range;
    }

    private int getRandom() {
        return new Random().nextInt(values.length);
    }

}
