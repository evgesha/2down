package kmr.game.fieldCreatorPackage;

public class Generator {

    public static int generateValue(int row) {
        return getRangeByRow(row).getValue();
    }

    private static Range getRangeByRow(int row) {
        for (Range range : Range.values()) {
            if (range.getRange() > row) {
                return range;
            }
        }
        return Range.LAST_LEVEL;
    }
}
