package kmr.game.fieldCreatorPackage;


import kmr.game.cell.Coordinate;
import kmr.game.game.Field;

public class CellFiller {

    public Field fill(Field field) {
        for (int row = 0; row < field.getSizeRow(); row++) {
            for (int column = 0; column < field.getSizeColumn(); column++) {
                if (field.isEmptyCell(row, column)) {
                    field.setSimpleCell(new Coordinate(row, column), Generator.generateValue(row));
                }
            }
        }
        return field;
    }
}
