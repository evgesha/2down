package kmr.game.fieldCreatorPackage;

import java.util.Random;

import kmr.game.cell.Coordinate;
import kmr.game.game.Field;

public class FieldCreator {

    private Random rand = new Random();
    private Field field;
    private int sizeRow;
    private int sizeColumn;

    public Field create(int sizeRow, int sizeColumn) {
        init(sizeRow, sizeColumn);
        setCellValue();
        setPlayerAndGoal();
        return field;
    }

    private void init(int sizeRow, int sizeColumn) {
        field = new Field(sizeRow, sizeColumn);
        this.sizeColumn = sizeColumn;
        this.sizeRow = sizeRow;
    }

    private void setPlayerAndGoal() {
        field.setPlayerCell(getCoordinatePlayer(), Generator.generateValue(1));
        field.setGoalCell(getCoordinateGoal(), Generator.generateValue(sizeRow));
    }

    private void setCellValue() {
        for (int row = 0; row < sizeRow; row++) {
            for (int column = 0; column < sizeColumn; column++) {
                field.setSimpleCell(new Coordinate(row, column), Generator.generateValue(row));
            }
        }
    }

    private Coordinate getCoordinatePlayer() {
        int x = rand.nextInt(2);
        int y = rand.nextInt(sizeColumn);
        return new Coordinate(x, y);
    }

    private Coordinate getCoordinateGoal() {
        int secondLineBottom = sizeRow - 2;
        int x = secondLineBottom + rand.nextInt(sizeRow - secondLineBottom);
        int y = rand.nextInt(sizeColumn);
        return new Coordinate(x, y);
    }

}
