package kmr.game.gameControll;

import kmr.game.game.Field;

public class MoveController {

    private Field field;

    public boolean canMove(Field field) {
        this.field = field;
        return checkColumns(field.getSizeColumn()) || checkRows(field.getSizeRow());
    }

    private boolean checkColumns(int sizeColumn) {
        for (int column = 0; column < sizeColumn; column++) {
            if (canGo(field.getColumn(column))) {
                return true;
            }
        }
        return false;
    }

    private boolean checkRows(int sizeRow) {
        for (int row = 0; row < sizeRow; row++) {
            if (canGo(field.getRow(row))) {
                return true;
            }
        }
        return false;
    }

    private boolean canGo(int[] column) {
        for (int index = 0; index < column.length - 1; index++) {
            if (column[index] == column[index + 1]) {
                return true;
            }
        }
        return false;
    }

}
