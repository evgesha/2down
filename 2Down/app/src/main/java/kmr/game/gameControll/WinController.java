package kmr.game.gameControll;

import kmr.game.cell.Coordinate;
import kmr.game.game.Field;

public class WinController {

    public boolean isWin(Field field) {
        Coordinate player = field.getCoordinatePlayer();
        Coordinate goal = field.getCoordinateGoalCell();
        return player.equals(goal);
    }
}
