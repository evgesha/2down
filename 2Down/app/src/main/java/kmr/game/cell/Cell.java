package kmr.game.cell;

import android.util.SparseIntArray;

public class Cell implements Cloneable {
    private int value;
    private SparseIntArray picture;
    private Color color;

    public Cell () {}

    public Cell(int value) {
        this.value = value;
    }

    public Cell(int value, Color color) {
        this.value = value;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setPicture(SparseIntArray array) {
        picture = array;
    }

    public int getPicture() {
        return picture.get(value);
    }

    public boolean equals(Cell obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(getClass() == obj.getClass())) {
            return false;
        } else {
            Cell exp = (Cell) obj;
            if (exp.value == this.value && exp.color == this.color) {
                return true;
            } else {
                return false;
            }
        }
    }

    public Cell Clone() throws
            CloneNotSupportedException {
        return (Cell) super.clone();
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
