package kmr.a2down.test.move;

import org.junit.BeforeClass;
import org.junit.Test;

import kmr.game.cell.Cell;
import kmr.game.cell.Coordinate;
import kmr.game.game.Field;
import kmr.game.moveOnMatriz.MatrixMove;
import kmr.game.moveOnMatriz.MatrixMoveDown;
import kmr.game.moveOnMatriz.MatrixMoveLeft;
import kmr.game.moveOnMatriz.MatrixMoveRight;
import kmr.game.moveOnMatriz.MatrixMoveUp;

import static kmr.game.cell.Color.Blue;
import static kmr.game.cell.Color.Red;

public class TestField {
    private static Field field;

    @BeforeClass
    public static void initField() {
        field = new Field(5, 5);
        field.setSimpleCell(new Coordinate(0, 0), 2);
        field.setGoalCell(new Coordinate(0, 1), 4);
        field.setSimpleCell(new Coordinate(0, 2), 4);
        field.setSimpleCell(new Coordinate(0, 3), 8);
        field.setSimpleCell(new Coordinate(0, 4), 8);

        field.setSimpleCell(new Coordinate(1, 0), 4);
        field.setSimpleCell(new Coordinate(1, 1), 4);
        field.setSimpleCell(new Coordinate(1, 2), 4);
        field.setSimpleCell(new Coordinate(1, 3), 8);
        field.setSimpleCell(new Coordinate(1, 4), 32);

        field.setSimpleCell(new Coordinate(2, 0), 8);
        field.setSimpleCell(new Coordinate(2, 1), 4);
        field.setPlayerCell(new Coordinate(2, 2), 4);
        field.setSimpleCell(new Coordinate(2, 3), 8);
        field.setSimpleCell(new Coordinate(2, 4), 2);

        field.setSimpleCell(new Coordinate(3, 0), 8);
        field.setSimpleCell(new Coordinate(3, 1), 2);
        field.setSimpleCell(new Coordinate(3, 2), 32);
        field.setSimpleCell(new Coordinate(3, 3), 8);
        field.setSimpleCell(new Coordinate(3, 4), 16);

        field.setSimpleCell(new Coordinate(4, 0), 8);
        field.setSimpleCell(new Coordinate(4, 1), 2);
        field.setSimpleCell(new Coordinate(4, 2), 4);
        field.setSimpleCell(new Coordinate(4, 3), 2);
        field.setSimpleCell(new Coordinate(4, 4), 16);
    }

    private static void printField() {
        for (int i = 0; i < field.getSizeRow(); i++) {
            for (int j = 0; j < field.getSizeColumn(); j++) {
                Cell object = field.getCell(i, j);
                if (object.getColor() == Blue) {
                    System.out.print(object.getValue() + 3 + " ");
                    continue;
                }
                if (object.getColor() == Red) {
                    System.out.print(object.getValue() + 1 + " ");
                    continue;
                }
                System.out.print(object.getValue() + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    @Test
    public void testReverse() {
        MatrixMove matrixMove = new MatrixMoveRight(field);
        printField();
        matrixMove.reverse();
        field = matrixMove.getField();
        printField();

    }

    @Test
    public void testTranspose() {
        MatrixMove matrixMove = new MatrixMoveRight(field);
        printField();
        matrixMove.transpose();
        field = matrixMove.getField();
        printField();

    }

    @Test
    public void testAmount() {
        MatrixMove matrixMove = new MatrixMoveRight(field);
        printField();
        matrixMove.amount();
        field = matrixMove.getField();
        printField();

    }

    @Test
    public void testShift() {
        MatrixMove matrixMove = new MatrixMoveRight(field);
        printField();
        matrixMove.colorShift(matrixMove.amount());
        field = matrixMove.getField();
        printField();
    }

    @Test
    public void testMoveRight() {
        MatrixMove matrixMove = new MatrixMoveRight(field);
        printField();
        matrixMove.move();
        field = matrixMove.getField();
        printField();
    }

    @Test
    public void testMoveLeft() {
        MatrixMove matrixMove = new MatrixMoveLeft(field);
        printField();
        matrixMove.move();
        field = matrixMove.getField();
        printField();
    }

    @Test
    public void testMoveDown() {
        MatrixMove matrixMove = new MatrixMoveDown(field);
        printField();
        matrixMove.move();
        field = matrixMove.getField();
        printField();
    }

    @Test
    public void testMoveUp() {
        MatrixMove matrixMove = new MatrixMoveUp(field);
        printField();
        matrixMove.move();
        field = matrixMove.getField();
        printField();
    }
}
